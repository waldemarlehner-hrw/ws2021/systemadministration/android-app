package de.rwu.remotepassword;

import static de.rwu.remotepassword.Constants.BT_RFCOMM_SOCKET_UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SelectDeviceActivity extends AppCompatActivity {

    private final static Logger logger = Logger.getLogger(SelectDeviceActivity.class.getName());
    private final static int REQUEST_ENABLE_BT = 0x1;
    private ConstraintLayout loadingLayout, noBluetoothLayout, resultLayout;

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, @NonNull Intent intent) {
            // This is not used as of now because no scan is started.
            String action = intent.getAction();
            if(BluetoothDevice.ACTION_FOUND.equals(action)) {
                // https://developer.android.com/guide/topics/connectivity/bluetooth/find-bluetooth-devices#java
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                logger.log(Level.FINE, "Found device "+device.getName()+"/ HWID: "+device.getAddress());
                SelectDeviceActivity.this.foundDevices.put(device.getAddress(), device);

                SelectDeviceActivity.this.updateUI();
            }
        }
    };

    private void updateUI() {
        this.resultLayout.setVisibility(View.GONE);
        this.loadingLayout.setVisibility(View.GONE);
        this.noBluetoothLayout.setVisibility(View.GONE);
        if(foundDevices.size() > 0) {
            this.resultLayout.setVisibility(View.VISIBLE);
        }
        else {
            this.loadingLayout.setVisibility(View.VISIBLE);
        }
        this.foundDevicesUniqueList.clear();
        foundDevicesUniqueList.addAll(foundDevices.values());
        this.resultAdapter.notifyDataSetChanged();
    }

    private final Hashtable<String, BluetoothDevice> foundDevices = new Hashtable<>();
    private final ArrayList<BluetoothDevice> foundDevicesUniqueList = new ArrayList<>();
    private ResultAdapter resultAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_device);
        this.resultAdapter = new ResultAdapter(this, 0);
        this.noBluetoothLayout = findViewById(R.id.errorBtDisabled);
        this.loadingLayout = findViewById(R.id.loadingBtDevices);
        this.resultLayout = findViewById(R.id.btDevicesResultLayout);
        ListView resultListview = findViewById(R.id.resultListview);
        resultListview.setAdapter(this.resultAdapter);
        resultListview.setOnItemClickListener(this::handleItemClick);

        IntentFilter bluetoothDeviceDiscoveredIntentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        super.registerReceiver(receiver,bluetoothDeviceDiscoveredIntentFilter);


        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bluetoothAdapter == null) {
            Toast.makeText(this, "Failed to get Bluetooth Adapter. Does this device support bluetooth?", Toast.LENGTH_LONG).show();
            return;
        }

        if(!bluetoothAdapter.isEnabled()) {
            Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BT);
        }
        else
        {
            this.initiateBluetoothScan();
        }
    }

    private void handleItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        BluetoothDevice selectedDevice = this.foundDevicesUniqueList.get(i);
        BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
        // Connect to device and check if it supports protocol.
        try {
            BluetoothSocket socket = selectedDevice.createRfcommSocketToServiceRecord(BT_RFCOMM_SOCKET_UUID);
            Toast.makeText(super.getApplicationContext(), "Socket created:" + socket.getRemoteDevice().getName(), Toast.LENGTH_LONG).show();
            socket.connect();
            DataStateSingleton.Instance().notifyAboutNewSocket(socket);
            DataStateSingleton.Instance().notifyAboutNewDevice(selectedDevice);
            finish();
        } catch (IOException e) {
            DataStateSingleton.Instance().notifyAboutNoDeviceSelected();
            e.printStackTrace();
            Toast.makeText(super.getApplicationContext(), "Failed to establish connection. Are you sure the selected device is a Password Device?", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_ENABLE_BT) {
              if(resultCode == RESULT_OK) {
                this.initiateBluetoothScan();
              }
              else if(resultCode == RESULT_CANCELED) {
                  Toast.makeText(this, "This app requires you to enable Bluetooth.", Toast.LENGTH_LONG).show();
                  finish();
              }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initiateBluetoothScan() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        this.foundDevices.clear();

        // Get already paired devices
        for(BluetoothDevice device : adapter.getBondedDevices()){
            this.foundDevices.put(device.getAddress(), device);
        }

        this.updateUI();
        // Find new Devices

        /// removed for now. only show already paired devices.
        //adapter.startDiscovery();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
    }

    private class ResultAdapter extends ArrayAdapter<BluetoothDevice> {

        public ResultAdapter(@NonNull Context context, int resource) {
            super(context, resource, SelectDeviceActivity.this.foundDevicesUniqueList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            BluetoothDevice device = SelectDeviceActivity.this.foundDevicesUniqueList.get(position);
            // https://guides.codepath.com/android/Using-an-ArrayAdapter-with-ListView
            if(convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_device_result, parent, false);
            }
            TextView name = convertView.findViewById(R.id.deviceName);
            TextView id = convertView.findViewById(R.id.deviceId);
            name.setText(device.getName());
            id.setText(device.getAddress());

            return convertView;
        }
    }
}