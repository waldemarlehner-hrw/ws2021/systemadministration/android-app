package de.rwu.remotepassword;

@FunctionalInterface
public interface NewInboundPacketReceivedEvent {
    void handleNewInboundPackedReceivedEvent(byte[] data);
}
