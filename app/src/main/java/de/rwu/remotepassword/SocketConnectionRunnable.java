package de.rwu.remotepassword;

import android.bluetooth.BluetoothSocket;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import de.waldemarlehner.javaListenerUtils.Action;

public class SocketConnectionRunnable implements Runnable {

    private final OutputStream out;
    private final InputStream in;
    private final NewInboundPacketReceivedEvent callback;
    private boolean isRunning;

    public SocketConnectionRunnable(@NonNull BluetoothSocket sock, @NonNull NewInboundPacketReceivedEvent callback) throws IOException {
        if(!sock.isConnected()) {
            sock.connect();
        }

        this.in = sock.getInputStream();
        this.out = sock.getOutputStream();
        this.callback = callback;
    }

    @Override
    public void run() {
        // https://developer.android.com/guide/topics/connectivity/bluetooth/transfer-data?hl=en
        byte[] buffer = new byte[1024];
        this.isRunning = true;
        while(this.isRunning) {
            try {
                int receivedBytes = this.in.read(buffer);
                if(receivedBytes == 0) {
                    continue;
                }
                byte[] packet = new byte[receivedBytes];
                System.arraycopy(buffer, 0, packet, 0, receivedBytes);
                this.callback.handleNewInboundPackedReceivedEvent(packet);
            } catch (IOException e) {
                this.isRunning = false;
                e.printStackTrace();
            }
        }
        try {
            this.in.close();
            this.out.flush();
            this.out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void notifyAboutTeardown() {
        this.isRunning = false;
    }

    public void writeData(byte[] data) throws IOException {
        this.out.write(data);
    }
}
