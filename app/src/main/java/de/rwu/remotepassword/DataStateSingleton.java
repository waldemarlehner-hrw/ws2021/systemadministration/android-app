package de.rwu.remotepassword;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.IOException;

import de.waldemarlehner.javaListenerUtils.Action;
import de.waldemarlehner.javaListenerUtils.ITopic;
import de.waldemarlehner.javaListenerUtils.TopicImpl;

public class DataStateSingleton {

    private DataStateSingleton() {
    }

    private static DataStateSingleton _instance;

    @NonNull
    public static DataStateSingleton Instance() {
        if (_instance == null) {
            _instance = new DataStateSingleton();
        }
        return _instance;
    }


    @Nullable
    private SocketState socket;
    private boolean currentSocketState;
    private final TopicImpl<Action.OneArg<Boolean>> socketConnectedTopic = new TopicImpl<>();
    private final TopicImpl<Action.OneArg<byte[]>> newDataReceivedTopic = new TopicImpl<>();
    @Nullable
    private BluetoothDevice device;

    public void notifyAboutNewDevice(@NonNull BluetoothDevice device) {
        this.device = device;
    }

    public void notifyAboutNoDeviceSelected() {
        this.device = null;
        if (this.socket != null) {
            if (this.socket.isActive()) {
                this.socket.dispose();
            }
            this.socket = null;
        }
    }

    @Nullable
    public BluetoothDevice getSelectedDevice() {
        return this.device;
    }

    public void notifyAboutNoSocketActive() {
        if(this.socket != null) {
            this.socket.dispose();
            this.notifyAboutNewSocketState(false);
        }
        this.socket = null;
    }

    public void notifyAboutNewSocket(@NonNull BluetoothSocket btSock) {
        try {
            this.socket = new SocketState(btSock);
            this.notifyAboutNewSocketState(this.socket.isActive());
            this.socket.getNewDataReceivedTopic().subscribe(data -> this.newDataReceivedTopic.fireListeners(l -> l.invoke(data)));
            this.socket.getSocketStateChangeTopic().subscribe(data -> this.socketConnectedTopic.fireListeners(l -> l.invoke(data)));
            this.socketConnectedTopic.fireListeners(l -> l.invoke(this.socket.isActive()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void notifyAboutNewSocketState(boolean connected) {
        if (this.currentSocketState != connected) {
            this.socketConnectedTopic.fireListeners(l -> l.invoke(connected));
            this.currentSocketState = connected;
        }
    }

    @NonNull
    public ITopic<Action.OneArg<Boolean>> getSocketConnectedTopic() {
        return this.socketConnectedTopic;
    }

    @Nullable
    public SocketState getSocketState() {
        return this.socket;
    }
}
