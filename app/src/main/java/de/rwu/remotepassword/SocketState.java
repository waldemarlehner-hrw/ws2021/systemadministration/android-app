package de.rwu.remotepassword;

import android.bluetooth.BluetoothSocket;

import androidx.annotation.NonNull;

import java.io.IOException;

import de.waldemarlehner.javaListenerUtils.Action;
import de.waldemarlehner.javaListenerUtils.ITopic;
import de.waldemarlehner.javaListenerUtils.TopicImpl;

public class SocketState {
    private final BluetoothSocket socket;
    private final TopicImpl<Action.OneArg<byte[]>> newDataReceivedTopic = new TopicImpl<>();

    private final Thread socketReceiveThread;
    private final SocketConnectionRunnable socketReceiveRunnable;
    private final TopicImpl<Action.OneArg<Boolean>> socketStateChangeTopic = new TopicImpl<>();

    public SocketState(@NonNull BluetoothSocket socket) throws IOException {
        this.socket = socket;
        this.socketReceiveRunnable = new SocketConnectionRunnable(socket, this::handleCallback);
        this.socketReceiveThread = new Thread(this.socketReceiveRunnable);
        this.socketReceiveThread.start();
        this.socketStateChangeTopic.fireListeners(l -> l.invoke(true));
    }

    private void handleCallback(byte[] bytes) {
        this.newDataReceivedTopic.fireListeners(l -> l.invoke(bytes));
    }

    public ITopic<Action.OneArg<byte[]>> getNewDataReceivedTopic() {
        return this.newDataReceivedTopic;
    }

    public boolean isActive() {
        return this.socket.isConnected();
    }

    public void dispose() {
        this.newDataReceivedTopic.clear();
        this.socketReceiveRunnable.notifyAboutTeardown();
        if(this.isActive()) {
            try {
                this.socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.socketStateChangeTopic.fireListeners(l -> l.invoke(false));
        this.socketStateChangeTopic.clear();
        this.socketReceiveThread.interrupt();
    }

    public ITopic<Action.OneArg<Boolean>> getSocketStateChangeTopic() {
        return this.socketStateChangeTopic;
    }

    public void write(byte[] bytes) throws IOException {
        this.socketReceiveRunnable.writeData(bytes);
    }
}
