package de.rwu.remotepassword;

import static de.rwu.remotepassword.Constants.BT_RFCOMM_SOCKET_UUID;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.switchmaterial.SwitchMaterial;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    Button changeDeviceButton;
    TextView selectedDeviceText;
    Button sendDataButton;
    EditText input;
    Switch socketConnectionSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.changeDeviceButton = findViewById(R.id.changeDeviceButton);
        this.changeDeviceButton.setOnClickListener(this::onChangeDeviceButtonClicked);
        this.selectedDeviceText = findViewById(R.id.selectedDeviceName);
        this.input = findViewById(R.id.inputPasswordEditText);
        this.sendDataButton = findViewById(R.id.inputSendButton);
        this.sendDataButton.setOnClickListener(this::handleSendDataButtonClick);
        this.socketConnectionSwitch = findViewById(R.id.connectedSwitch);
        this.socketConnectionSwitch.setOnCheckedChangeListener(this::handleConnectionSwitchChange);

        DataStateSingleton.Instance().getSocketConnectedTopic().subscribe(this::handleSocketStateChange);
    }

    private void handleSocketStateChange(Boolean newState) {
        this.sendDataButton.setEnabled(newState);
        this.input.setEnabled(newState);
        this.socketConnectionSwitch.setChecked(newState);
        this.socketConnectionSwitch.setEnabled(true);
        this.changeDeviceButton.setEnabled(!newState);
    }

    private void handleConnectionSwitchChange(CompoundButton compoundButton, boolean b) {
        SocketState state = DataStateSingleton.Instance().getSocketState();
        if(state != null && state.isActive() != b){
            this.socketConnectionSwitch.setEnabled(false);
            try{
                @Nullable BluetoothDevice dev = DataStateSingleton.Instance().getSelectedDevice();
                if(b) {
                    if(dev != null) {
                        BluetoothSocket socket = dev.createRfcommSocketToServiceRecord(BT_RFCOMM_SOCKET_UUID);
                        DataStateSingleton.Instance().notifyAboutNewSocket(socket);
                    }
                    else {
                        this.socketConnectionSwitch.setEnabled(true);
                        Toast.makeText(getApplicationContext(), "Select a Device first", Toast.LENGTH_SHORT).show();
                    }

                }
                else{
                    state.dispose();
                }
            }
            catch (IOException ex) {
                Toast.makeText(getApplicationContext(), "Failed to enable/disable Socket", Toast.LENGTH_SHORT).show();
            }

        }
        else if(state == null){
            this.socketConnectionSwitch.setEnabled(true);
            Toast.makeText(getApplicationContext(), "No Socket found. Have you selected a device?", Toast.LENGTH_SHORT).show();
            this.socketConnectionSwitch.setChecked(false);

        }


    }

    private void handleSendDataButtonClick(View view) {

        String inputText = this.input.getText().toString().trim();
        if(inputText.length() == 0) {
            return;
        }

        this.input.setText("");

        @Nullable
        SocketState socketState = DataStateSingleton.Instance().getSocketState();
        if(socketState == null) {
            Toast.makeText(this.getApplicationContext(), "Socket is not set", Toast.LENGTH_SHORT).show();
            return;
        }

        if(socketState.isActive()) {
            try {
                socketState.write(inputText.getBytes(StandardCharsets.UTF_8));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.updateUIState();
        @Nullable BluetoothDevice device = DataStateSingleton.Instance().getSelectedDevice();
        if(device != null) {
            if(DataStateSingleton.Instance().getSocketState() == null) {
                try {
                    BluetoothSocket socket = device.createRfcommSocketToServiceRecord(BT_RFCOMM_SOCKET_UUID);
                    socket.connect();
                    DataStateSingleton.Instance().notifyAboutNewSocket(socket);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            SocketState state = DataStateSingleton.Instance().getSocketState();
            if(state != null) {
                state.dispose();
                DataStateSingleton.Instance().notifyAboutNoSocketActive();
            }
        }
    }

    private void handleInboundData(byte[] bytes) {
        super.runOnUiThread(() -> {
            String data = new String(bytes, StandardCharsets.UTF_8);
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Response").setMessage("Received: "+ data).setPositiveButton("Ok", (dialog, which) -> { });
            builder.show();
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void updateUIState() {
        @Nullable BluetoothDevice selectedDevice = DataStateSingleton.Instance().getSelectedDevice();
        String text = selectedDevice == null ? "None Selected" : selectedDevice.getName();
        this.selectedDeviceText.setText(text);
        @Nullable SocketState sockState = DataStateSingleton.Instance().getSocketState();
        if(sockState == null) {
            this.handleSocketStateChange(false);
        }
        else {
            this.handleSocketStateChange(sockState.isActive());
        }

    }

    private void onChangeDeviceButtonClicked(View view) {
        Intent intent = new Intent(this, SelectDeviceActivity.class);
        startActivity(intent);
    }

}